//#region Constant, Data
var settings = {
    wave: 1000.0,
    gui: false,
    ambient: true,
    model: "",
    setup: "2.json",
    scale: 0.01,
    position: {
        x: 0,
        y: 0,
        z: 0
    },
    background: "#9d162e",
    logMeshName: false,
    logIntersects: true,
    exposure: 1,
    camera: {
        fov: 62,
        x: -7.066248672326327, y: 25.915911081245536, z: 30.7414472858804
    },

    material: {
        lightMapIntensity: 1,
        envMapIntensity: 1,
        aoMapIntensity: 0,
        emissiveIntensity: 0,
        envMapIntensity: 0.1,
        roughness: 1,
        metalness: 0
    },
    //Camera target / Pivot target
    target: {
        x: 3.061869445366514, y: 0, z: -6.023349728589866
    },
    //Orbit-control
    minDistance: 0.1,
    maxDistance: 200,
    minPolarAngle: 0,
    maxPolarAngle: Math.PI / 2, //infinity
    minAzimuthAngle: Math.PI / 2,
    maxAzimuthAngle: Math.PI / 1,
    autoRotate: false,
    autoRotateSpeed: 0.03,
    //renderer
    gammaOutput: false
}

//List sensor poistion and respect coordinate include: x, y, z
var positions = [
    { room: "1B7S", sensor_id: "508389a7-dbdc-11e8-b063-ae3c12f3bdb7", position: { x: -12.572, y: 1.11862, z: -5.0} },

    { room: "1B1R", sensor_id: "8cf7864b-9bd7-11e8-862f-ae3c12f3bdb7", position: { x: -12.2, y: 0.018624002933502193, z: 5.5 } },
    { room: "1B1Q", sensor_id: "8916977e-9bd8-11e8-862f-ae3c12f3bdb7", position: { x: -10.101666179645896, y: 0.018624002933502193, z: 6.2 } },
    { room: "1B1P", sensor_id: "68bec80b-dbdc-11e8-b063-ae3c12f3bdb7", position: { x: -7.856468038870391, y: 0.018624002933502193, z: 6.812486176031297 } },
    { room: "1B1N", sensor_id: "a1aeb4f1-9bd7-11e8-862f-ae3c12f3bdb7", position: { x: -5.361803480543723, y: 0.018624002933516404, z: 7.43615226086977 } },
    { room: "1B1M", sensor_id: "a6657e83-9bd8-11e8-862f-ae3c12f3bdb7", position: { x: -2.867138958196617, y: 0.018624002933502193, z: 8.1 } },
    { room: "1B1L", sensor_id: "dc0a4516-4ec6-11e8-bbb5-9a61bd9e0b11", position: { x: -0.49720773358349124, y: 0.018624002933502193, z: 8.68348439440194 } },
    { room: "1B1K", sensor_id: "396a4082-51c7-11e8-bbb5-9a61bd9e0b11", position: { x: 1.810356836907542, y: 0.018624002933502193, z: 9.2 } },
    { room: "1A6R", sensor_id: "19", position: { x: 8.679322831843123, y: 0.018624002933502193, z: -4.330205613188829 } },
    { room: "1A6R", sensor_id: "19_2", position: { x: 20.003751108373578, y: 0.018624002933502193, z: -20.901830370254316 } }
];

//Detail infomation of sensor
var apidata = [
{
    "sensor_id": "508389a7-dbdc-11e8-b063-ae3c12f3bdb7",
    "sensortype_id": "9",
    "attributes": [
        "temperature",
        "humidity",
        "co2"
    ],
    "device_eui": "000db5360b6d3663",
    "name": "CO2-677",
    "reported_at": "2019-06-27T13:30:34+0200",
    "favorite": true,
    "last_measurement": {
        "reported_at": "2019-06-27 13:30:34",
        "temperature": 25.53,
        "humidity": 77,
        "co2": 577
    },
    "status": "disconnected"
}, 
{
    "sensor_id": "8cf7864b-9bd7-11e8-862f-ae3c12f3bdb7",
    "sensortype_id": "8",
    "attributes": [
        "temperature",
        "humidity",
        "co2"
    ],
    "device_eui": "000db5360b6d3663",
    "name": "CO2-663",
    "reported_at": "2019-06-27T13:30:34+0200",
    "favorite": true,
    "last_measurement": {
        "reported_at": "2019-06-27 13:30:34",
        "temperature": 22.53,
        "humidity": 44,
        "co2": 532
    },
    "status": "disconnected"
}, 
{ "sensor_id": "8916977e-9bd8-11e8-862f-ae3c12f3bdb7", "sensortype_id": "8", "attributes": ["temperature", "humidity", "co2"], "device_eui": "000db5350f74377a", "name": "CO2-77A", "lat": "28.6134590", "lng": "-115.9835850", "reported_at": "2020-07-03T06:14:49+0200", "favorite": true, "last_measurement": { "reported_at": "2020-07-03 06:14:49", "temperature": 21.300000000000001, "humidity": 34, "co2": 413 }, "status": "online" }, { "sensor_id": "a1aeb4f1-9bd7-11e8-862f-ae3c12f3bdb7", "sensortype_id": "9", "attributes": ["temperature", "humidity", "motion"], "device_eui": "70b3d5ee10060285", "name": "PIR-285", "reported_at": "2020-07-03T05:03:20+0200", "favorite": true, "last_measurement": { "reported_at": "2020-07-03 05:03:20", "temperature": 21.559999999999999, "humidity": 30, "motion": false, "jsondata": { "number_of_free_blocks": 31, "number_of_occupied_blocks": 30, "number_of_motion_events_in_block": 0 } }, "status": "online" }, { "sensor_id": "a6657e83-9bd8-11e8-862f-ae3c12f3bdb7", "sensortype_id": "9", "attributes": ["temperature", "humidity", "motion"], "device_eui": "70b3d5ee1006028a", "name": "PIR-28A", "reported_at": "2020-07-03T00:54:37+0200", "favorite": true, "last_measurement": { "reported_at": "2020-07-03 00:54:37", "temperature": 21.059999999999999, "humidity": 44, "motion": false, "jsondata": { "number_of_free_blocks": 15, "number_of_occupied_blocks": 14, "number_of_motion_events_in_block": 0 } }, "status": "delayed" }, { "sensor_id": "dc0a4516-4ec6-11e8-bbb5-9a61bd9e0b11", "sensortype_id": "10", "attributes": ["temperature", "humidity", "co2", "light", "motion", "noise"], "device_eui": "10.0.0.12_0.13.3.0", "name": "Sensor 13", "country": "Norge", "city": "Oslo", "zip": "N-0380", "street": "Silurveien 2", "description": "Selvaag 13 , 10.0.0.12_0.13.3.0", "reported_at": "2019-03-28T23:57:08+0100", "favorite": true, "last_measurement": { "reported_at": "2019-03-28 23:57:08", "temperature": 22.239999999999998, "humidity": 26, "motion": false, "co2": 471, "light": 56, "noise": 36.289999999999999 }, "status": "disconnected" }, { "sensor_id": "396a4082-51c7-11e8-bbb5-9a61bd9e0b11", "sensortype_id": "10", "attributes": ["temperature", "humidity", "co2", "light", "motion", "noise"], "device_eui": "10.0.0.12_0.15.3.0", "name": "Sensor 15", "country": "Norge", "city": "Oslo", "zip": "N-0380", "street": "Silurveien 2", "description": "10.0.0.12_0.15.3.0", "reported_at": "2019-01-21T23:25:34+0100", "favorite": true, "last_measurement": { "reported_at": "2019-01-21 23:25:34", "temperature": 22.09, "humidity": 18, "motion": false, "co2": 521, "light": 0, "noise": 38.759999999999998 }, "status": "disconnected" }];

var green = new THREE.Color("green"),
    yellow = new THREE.Color("yellow"),
    red = new THREE.Color("#f24d39");

let testData = apidata.find(x => x.name.includes("CO2-77A")).last_measurement.co2 = 800;
//#endregion

//#region Global variable
var redSphere, 
    webGLRender, 
    orbitControl, 
    projector;
var defaultScene;
var B = [],  
    listSensor = [];
//#endregion

var funcCreateTextureLoader = createTextureLoader("CUBE", "image");
function createTextureLoader(name, folder) {
    if ("CUBE" === name) {
        var t = folder + "/";
        return (new THREE.CubeTextureLoader).load([t + "posx.jpg", t + "negx.jpg", t + "posy.jpg", t + "negy.jpg", t + "posz.jpg", t + "negz.jpg"])
    }
    return "SPHERE" === name ? (t = (new THREE.TextureLoader).load(folder),
        t.mapping = THREE.SphericalReflectionMapping,
        t) : !1
}

function preventBehavior(e) {
    e.preventDefault();
};
document.addEventListener("touchmove", preventBehavior, false);


var loadingManager = new THREE.LoadingManager();
loadingManager.onStart = function (url, itemsLoaded, itemsTotal) {
    console.log('Started loading file: ' + url + '.\nLoaded ' + itemsLoaded + ' of ' + itemsTotal + ' files.');
};
loadingManager.onLoad = function () {
    console.log('Loading complete!');
};
loadingManager.onProgress = function (url, itemsLoaded, itemsTotal) {
    console.log('Loading file: ' + url + '.\nLoaded ' + itemsLoaded + ' of ' + itemsTotal + ' files.');
};
loadingManager.onError = function (url) {
    console.log('There was an error loading ' + url);
};

function getLoader(objLoader, onProgress) {
    return {
        originalLoader: objLoader,
        load: function (url) {
            return new Promise(function (onLoad, onError) {
                objLoader.load(url, onLoad, onProgress, onError)
            }
            )
        }
    }
}
var loader = getLoader(new THREE.OBJLoader(loadingManager));

var O;
var container;
var perspectiveCamera;
var bubIndex = 0;

init();
animate();

/*Setup 
 - The secene: background, add content such as: light, objects
 - Camera type and default position
 - Load obj from .obj file, process child base on its name, map original image 2D on bottom
 - Load sensor infomation and add into scene
*/
function init() {
    container = document.createElement('div');
    document.body.appendChild(container);

    perspectiveCamera = new THREE.PerspectiveCamera(settings.camera.fov, window.innerWidth / window.innerHeight, .01, 2e3);
    perspectiveCamera.position.set(settings.camera.x, settings.camera.y, settings.camera.z);

    defaultScene = new THREE.Scene();
    defaultScene.background = new THREE.Color("#ffffff");
    var directionalLight = new THREE.DirectionalLight(16777215, .2, 50);
    directionalLight.position.set(50, 120, 0);
    directionalLight.castShadow = !0;
    directionalLight.shadow.mapSize.width = 2048;
    directionalLight.shadow.mapSize.height = 2048;
    directionalLight.shadow.camera.near = .5;
    directionalLight.shadow.camera.far = 258;
    directionalLight.shadow.camera.left = -32;
    directionalLight.shadow.camera.right = 32;
    directionalLight.shadow.camera.top = 32;
    directionalLight.shadow.camera.bottom = -32;
    defaultScene.add(directionalLight);

    loader.load("three-dimension-object/obj/5.2.obj").then(function (e) {
        e.traverse(function (e) {
            e.isMesh &&
                (e.castShadow = !0,
                    e.receiveShadow = !0,
                    e.name.includes("Window") 
                        ? e.material = new THREE.MeshStandardMaterial({
                                                emissive: new THREE.Color("#6a17b6"),
                                                emissiveIntensity: 1,
                                                transparent: !0,
                                                opacity: .5,
                                                metalness: 1,
                                                roughness: 0
                                            }) 
                        : e.material = new THREE.MeshStandardMaterial({
                                            envMap: funcCreateTextureLoader,
                                            envMapIntensity: 3,
                                            metalness: .4,
                                            roughness: .6,
                                            color: new THREE.Color("#ffffff")
                                        }),
                    e.name.includes("top_Wall") && (e.material.color = new THREE.Color("#2b2b2b")),
                    "plan" == e.name && (e.material.map = new THREE.TextureLoader(loadingManager).load("image/plan_lowres.jpg"),
                        e.material.roughness = 1,
                        e.material.metalness = 0,
                        e.position.set(0, .12, 0)),
                    e.name.includes("Floor") && (e.visible = !1),
                    e.name.includes("Door") && (e.visible = !1),
                    e.name.includes("Mball") && (console.log(e))
                )
        });
        defaultScene.add(e);        
        setTimeout(function () {
            loadSensor();

        }, 100)
    })["catch"](function (e) {
        console.error(e)
    });


    redSphere = new THREE.Mesh(
        new THREE.SphereGeometry(.4, 32, 32), 
        new THREE.MeshBasicMaterial({
            color: 65535
        })
    );
    redSphere.castShadow = !0;
    redSphere.receiveShadow = !0;
    
    var axesHelper = new THREE.AxesHelper(50);
    defaultScene.add( axesHelper );

    var tempMesh = redSphere.clone();
    tempMesh.position.set(0, 2, 0);
    defaultScene.add(tempMesh); 

    

    webGLRender = new THREE.WebGLRenderer({
        antialias: !0,
        preserveDrawingBuffer: !0,
        alpha: !1
    });
    if (settings.ambient) {
        var n = new THREE.AmbientLight(16777215, .9);
        defaultScene.add(n)
    }
    projector = new THREE.Projector;
    webGLRender.setPixelRatio(window.devicePixelRatio);
    webGLRender.setSize(window.innerWidth, window.innerHeight);
    webGLRender.shadowMap.enabled = !0;
    webGLRender.gammaOutput = settings.gammaOutput;
    webGLRender.toneMappingExposure = settings.exposure;
    webGLRender.renderReverseSided = !0;
    webGLRender.localClippingEnabled = !0;

    container.appendChild(webGLRender.domElement);

    orbitControl = new THREE.OrbitControls(perspectiveCamera, webGLRender.domElement);
    orbitControl.target.set(settings.target.x, settings.target.y, settings.target.z);
    orbitControl.minDistance = settings.minDistance;
    orbitControl.maxDistance = settings.maxDistance;
    orbitControl.minPolarAngle = settings.minPolarAngle;
    orbitControl.maxPolarAngle = settings.maxPolarAngle;
    orbitControl.screenSpacePanning = !1;
    settings.restrictOrbit && (orbitControl.minAzimuthAngle = settings.minAzimuthAngle,
        orbitControl.maxAzimuthAngle = settings.maxAzimuthAngle);
    settings.autoRotate && (orbitControl.autoRotate = !0);
    settings.autoRotateSpeed && (orbitControl.autoRotateSpeed = settings.autoRotateSpeed);
    orbitControl.update();

    window.addEventListener("resize", resize, !1);
    window.addEventListener("click", windowClick, !1);

};

function animate(e) {
    requestAnimationFrame(animate);
    webGLRender.render(defaultScene, perspectiveCamera);
    TWEEN.update(e);
    orbitControl.update()
}

/* Get sensor correspond with sensor in api data.
 If it existed: 
 + Create a red sphere by using mesh and push into two list B[], H[]
 + Generate list sensor and append to sensorsElement
*/
function loadSensor() {
    var r = 0, sensorsElement = $("#sensors");
    setTimeout(function () {
        positions.forEach(function (pos) {
            var sensor = apidata.find(function (s) {
                return s.sensor_id == pos.sensor_id
            });
            if (sensor) {
                var mesh = redSphere.clone();
                mesh.position.set(pos.position.x, 2, pos.position.z);
                mesh.userData = sensor;
                mesh.userData.position = pos.position;
                mesh.userData.room = pos.room;
                mesh.userData.index = r;
                var a = ""
                    , color = green
                    , o = "";
                
                sensor.last_measurement.co2 && 700 < sensor.last_measurement.co2 
                && (color = red,
                    o = "warn",
                    a = ' <i class="fa fa-exclamation-triangle"></i>');

                mesh.userData.originalColor = color;
                mesh.material = new THREE.MeshLambertMaterial({
                    color: color
                });
                defaultScene.add(mesh);
                //B.push(mesh);
                listSensor.push(mesh);
                sensorsElement.append("<li data-index='" + r + "' class=" + o + '><i class="fa fa-circle ok"></i> ' + sensor.name + a + "</li>");
                r++
            }
        });
        $("#warningList").slideDown(900)
    }, 100)
};

/*
    - Translating from screen coordinates to cartesian coordinate in 3D world
    - Put this poistion below camera viewpoint
    - Create a dicrection from camera position and mouse position. If the direction intersect any sensor => active it.
*/
function windowClick(e) {
    var currentPosition = new THREE.Vector3(e.clientX / window.innerWidth * 2 - 1, 2 * -(e.clientY / window.innerHeight) + 1, .5);
    projector.unprojectVector(currentPosition, perspectiveCamera); //vector.unproject(camera)
    currentPosition.sub(perspectiveCamera.position);
    currentPosition.normalize();
    var matchBubs = new THREE.Raycaster(perspectiveCamera.position, currentPosition).intersectObjects(listSensor, !0);
    var bub;
    0 < matchBubs.length && settings.logIntersects && (bub = matchBubs[0].object,
        setActiveBubMaterialColor(bub),
        activeSensor(bub.userData))
}


function setActiveBubMaterialColor(sensor) {
    listSensor.forEach(function (s) {
        s.material.color = s.userData.originalColor
    });
    sensor.material.color = yellow;
}

/*
- Active sensor in list
- Change (tween) position of sensor and camera
 */
function activeSensor(sensor) {
    bubIndex = sensor.index;
    $("#sensors li").removeClass("active");
    $("#sensors li").eq(bubIndex).addClass("active");    
    sensor.attributes || (sensor = getSensorById(sensor.sensor_id));
    displaySensorInfo(sensor);
    
    var sensorPosition = sensor.position;
    var tweenSensor = new TWEEN.Tween(orbitControl.target)
    .to(sensorPosition, 700)
    .onComplete(function () { })
    .easing(TWEEN.Easing.Cubic.Out);

    var tweenCamera = new TWEEN.Tween(perspectiveCamera.position)
    .to({
        x: sensorPosition.x - 3.6,
        y: sensorPosition.y + 18,
        z: sensorPosition.z + 14
    }, 700)
    .onStart(function () {
        orbitControl.enabled = !1
    })
    .onComplete(function () {
        orbitControl.enabled = !0
    })
    .easing(TWEEN.Easing.Cubic.Out);

    tweenSensor.start();
    tweenCamera.start()
}

function displaySensorInfo(sensor) {
    var n = $("#datadisplay");
    n.is(":hidden") && n.fadeIn();
    n.find(".device-info").html(sensor.name);
    n.find(".room-info").html(sensor.room);
    var t = "<table>";
    t += "<tr>";
    var a = Object.keys(sensor.last_measurement);
    for (var propIndex in a)
        t += "<td>" + a[propIndex] + "</td>";
    t += "</tr>";
    t += "<tr>";
    Object.values(sensor.last_measurement).forEach(function (e) {
        t = "object" == typeof e ? t + ("<td>" + typeof e + "</td>") : 700 < e ? t + ('<td class="data-warning">' + e + "</td>") : t + ("<td>" + e + "</td>")
    });
    t += "</tr>";
    t += "</table>";
    n.find(".data-container").html(t)
}
function getSensorById(id) {
    var sensor = apidata.find(function (e) {
        return e.sensor_id == id
    });
    if (sensor)
        return sensor
}

$("#closedisplay").click(function () {
    $("#datadisplay").fadeOut()
});

function resize() {
    perspectiveCamera.aspect = window.innerWidth / window.innerHeight;
    perspectiveCamera.updateProjectionMatrix();
    webGLRender.setSize(window.innerWidth, window.innerHeight)
}


$("#sensors").on("click", "li", function () {
    var index = $(this).attr("data-index")
        , position = positions[index];
    bubIndex = index;
    position.index = bubIndex;
    setActiveBubMaterialColor(listSensor[bubIndex]);
    activeSensor(position)
});