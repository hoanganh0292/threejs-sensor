import * as THREE from "../node_modules/three/build/three.module.js";
import {OrbitControls} from '../node_modules/three/examples/jsm/controls/OrbitControls.js';
import {GLTFLoader} from '../node_modules/three/examples/jsm/loaders/GLTFLoader.js';
import {Projector} from '../node_modules/three/examples/jsm/renderers/Projector.js';
//#region Constant, Data
var settings = {
    wave: 1000.0,
    gui: false,
    ambient: true,
    model: "",
    setup: "2.json",
    scale: 0.01,
    position: {
        x: 0,
        y: 0,
        z: 0
    },
    background: "#9d162e",
    logMeshName: false,
    logIntersects: true,
    exposure: 1,
    camera: {
        fov: 70,
        x: -51.218628817343074,
        y: 39.011416770452634,
        z: -42.437661389731986
    },

    material: {
        lightMapIntensity: 1,
        envMapIntensity: 1,
        aoMapIntensity: 0,
        emissiveIntensity: 0,
        envMapIntensity: 0.1,
        roughness: 1,
        metalness: 0
    },
    //Camera target / Pivot target
    target: {
        x: 3.061869445366514, y: 0, z: -6.023349728589866
    },
    //Orbit-control
    minDistance: 0.1,
    maxDistance: 200,
    minPolarAngle: 0,
    maxPolarAngle: Math.PI / 2, //infinity
    minAzimuthAngle: Math.PI / 2,
    maxAzimuthAngle: Math.PI / 1,
    autoRotate: false,
    autoRotateSpeed: 0.03,
    //renderer
    gammaOutput: false
}

//List sensor poistion and respect coordinate include: x, y, z
var positions = [
    { room: "1B7S", sensor_id: "S0001", position: { x: 2.0 ,y: 2.25, z: -3.0 }, cameraLocation: {x: -1.7961500804975294, y: 1.1591256697753287, z: -2.822893843208729}},
    { room: "1B1R", sensor_id: "S0002", position: { x: 2.0 ,y: 2.25, z: -7.0}, cameraLocation: {x: -2.970606719431175, y: 1.972479043395389, z: -7.975022516181761}},
    { room: "1B1Q", sensor_id: "S0003", position: { x: 3.68 ,y: 2.25, z: -7.6 }, cameraLocation: {x: 5.62066942781809, y: 0.633282768653407, z: -8.787256906545705 }},    
    { room: "1B1N", sensor_id: "S0004", position: {x: 4.62 ,y: 2.34, z: -4.4}, cameraLocation: {x: 3.7523740617140593, y: 0.982967404950132, z: 0.5880723903025613}},   
];

//Detail infomation of sensor
var apidata = [ { "sensor_id": "S0001", "sensortype_id": "9", "attributes": [ "temperature", "humidity", "co2" ], "device_eui": "000db5360b6d3663", "name": "Meeting One", "reported_at": "2019-06-27T13:30:34+0200", "favorite": true, "last_measurement": { "reported_at": "2019-06-27 13:30:34", "temperature": 25.53, "humidity": 77, "co2": 577 }, "status": "disconnected" }, { "sensor_id": "S0002", "sensortype_id": "8", "attributes": [ "temperature", "humidity", "co2" ], "device_eui": "000db5360b6d3663", "name": "Meeting Two", "reported_at": "2019-06-27T13:30:34+0200", "favorite": true, "last_measurement": { "reported_at": "2019-06-27 13:30:34", "temperature": 22.53, "humidity": 44, "co2": 532 }, "status": "disconnected" }, { "sensor_id": "S0003", "sensortype_id": "8", "attributes": [ "temperature", "humidity", "co2" ], "device_eui": "000db5350f74377a", "name": "Small Room", "lat": "28.6134590", "lng": "-115.9835850", "reported_at": "2020-07-03T06:14:49+0200", "favorite": true, "last_measurement": { "reported_at": "2020-07-03 06:14:49", "temperature": 21.3, "humidity": 34, "co2": 413 }, "status": "online" }, { "sensor_id": "S0004", "sensortype_id": "9", "attributes": [ "temperature", "humidity", "motion" ], "device_eui": "70b3d5ee10060285", "name": "Main Room", "reported_at": "2020-07-03T05:03:20+0200", "favorite": true, "last_measurement": { "reported_at": "2020-07-03 05:03:20", "temperature": 21.56, "humidity": 30, "motion": false, "jsondata": { "number_of_free_blocks": 31, "number_of_occupied_blocks": 30, "number_of_motion_events_in_block": 0 } }, "status": "online" }, { "sensor_id": "S0005", "sensortype_id": "9", "attributes": [ "temperature", "humidity", "motion" ], "device_eui": "70b3d5ee1006028a", "name": "PIR-28A", "reported_at": "2020-07-03T00:54:37+0200", "favorite": true, "last_measurement": { "reported_at": "2020-07-03 00:54:37", "temperature": 21.06, "humidity": 44, "motion": false, "jsondata": { "number_of_free_blocks": 15, "number_of_occupied_blocks": 14, "number_of_motion_events_in_block": 0 } }, "status": "delayed" }, { "sensor_id": "S0006", "sensortype_id": "10", "attributes": [ "temperature", "humidity", "co2", "light", "motion", "noise" ], "device_eui": "10.0.0.12_0.13.3.0", "name": "Sensor 13", "country": "Norge", "city": "Oslo", "zip": "N-0380", "street": "Silurveien 2", "description": "Selvaag 13 , 10.0.0.12_0.13.3.0", "reported_at": "2019-03-28T23:57:08+0100", "favorite": true, "last_measurement": { "reported_at": "2019-03-28 23:57:08", "temperature": 22.24, "humidity": 26, "motion": false, "co2": 471, "light": 56, "noise": 36.29 }, "status": "disconnected" }, { "sensor_id": "S0007", "sensortype_id": "10", "attributes": [ "temperature", "humidity", "co2", "light", "motion", "noise" ], "device_eui": "10.0.0.12_0.15.3.0", "name": "Sensor 15", "country": "Norge", "city": "Oslo", "zip": "N-0380", "street": "Silurveien 2", "description": "10.0.0.12_0.15.3.0", "reported_at": "2019-01-21T23:25:34+0100", "favorite": true, "last_measurement": { "reported_at": "2019-01-21 23:25:34", "temperature": 22.09, "humidity": 18, "motion": false, "co2": 521, "light": 0, "noise": 38.76 }, "status": "disconnected" }, { "sensor_id": "S0008", "sensortype_id": "9", "attributes": [ "temperature", "humidity", "co2" ], "device_eui": "000db5360b6d3663", "name": "CO2-608", "reported_at": "2019-06-27T13:30:34+0200", "favorite": true, "last_measurement": { "reported_at": "2019-06-27 13:30:34", "temperature": 25.53, "humidity": 108, "co2": 508 }, "status": "disconnected" }, { "sensor_id": "S0009", "sensortype_id": "9", "attributes": [ "temperature", "humidity", "co2" ], "device_eui": "000db5360b6d3663", "name": "CO2-609", "reported_at": "2019-06-27T13:30:34+0200", "favorite": true, "last_measurement": { "reported_at": "2019-06-27 13:30:34", "temperature": 25.53, "humidity": 109, "co2": 509 }, "status": "disconnected" }, { "sensor_id": "S0010", "sensortype_id": "9", "attributes": [ "temperature", "humidity", "co2" ], "device_eui": "000db5360b6d3663", "name": "CO2-610", "reported_at": "2019-06-27T13:30:34+0200", "favorite": true, "last_measurement": { "reported_at": "2019-06-27 13:30:34", "temperature": 25.53, "humidity": 110, "co2": 510 }, "status": "disconnected" }, { "sensor_id": "S0011", "sensortype_id": "9", "attributes": [ "temperature", "humidity", "co2" ], "device_eui": "000db5360b6d3663", "name": "CO2-611", "reported_at": "2019-06-27T13:30:34+0200", "favorite": true, "last_measurement": { "reported_at": "2019-06-27 13:30:34", "temperature": 25.53, "humidity": 111, "co2": 511 }, "status": "disconnected" }, { "sensor_id": "S0012", "sensortype_id": "9", "attributes": [ "temperature", "humidity", "co2" ], "device_eui": "000db5360b6d3663", "name": "CO2-612", "reported_at": "2019-06-27T13:30:34+0200", "favorite": true, "last_measurement": { "reported_at": "2019-06-27 13:30:34", "temperature": 25.53, "humidity": 112, "co2": 512 }, "status": "disconnected" }, { "sensor_id": "S0013", "sensortype_id": "z10", "attributes": [ "temperature", "humidity", "co2" ], "device_eui": "000db5360b6d3663", "name": "CO2-613", "reported_at": "2019-06-27T13:30:34+0200", "favorite": true, "last_measurement": { "reported_at": "2019-06-27 13:30:34", "temperature": 25.53, "humidity": 113, "co2": 513 }, "status": "disconnected" }, { "sensor_id": "S0014", "sensortype_id": "z10", "attributes": [ "temperature", "humidity", "co2" ], "device_eui": "000db5360b6d3663", "name": "CO2-614", "reported_at": "2019-06-27T13:30:34+0200", "favorite": true, "last_measurement": { "reported_at": "2019-06-27 13:30:34", "temperature": 25.53, "humidity": 14, "co2": 514 }, "status": "disconnected" }, { "sensor_id": "S0015", "sensortype_id": "z10", "attributes": [ "temperature", "humidity", "co2" ], "device_eui": "000db5360b6d3663", "name": "CO2-615", "reported_at": "2019-06-27T13:30:34+0200", "favorite": true, "last_measurement": { "reported_at": "2019-06-27 13:30:34", "temperature": 25.53, "humidity": 15, "co2": 515 }, "status": "disconnected" } ];
var green = new THREE.Color("green"),
    yellow = new THREE.Color("yellow"),
    red = new THREE.Color("red");

//#endregion

//#region Global variable
var redSphere, 
    webGLRender, 
    orbitControl, 
    projector;
var defaultScene;
var listSensor = [];
//#endregion

function preventBehavior(e) {
    e.preventDefault();
};
document.addEventListener("touchmove", preventBehavior, false);


var loadingManager = new THREE.LoadingManager();
loadingManager.onStart = function (url, itemsLoaded, itemsTotal) {
    console.log('Started loading file: ' + url + '.\nLoaded ' + itemsLoaded + ' of ' + itemsTotal + ' files.');
};
loadingManager.onLoad = function () {
    console.log('Loading complete!');
};
loadingManager.onProgress = function (url, itemsLoaded, itemsTotal) {
    console.log('Loading file: ' + url + '.\nLoaded ' + itemsLoaded + ' of ' + itemsTotal + ' files.');
};
loadingManager.onError = function (url) {
    console.log('There was an error loading ' + url);
};

var container;
var perspectiveCamera;
var bubIndex = 0;

init();
animate();

/*Setup 
 - The secene: background, add content such as: light, objects
 - Camera type and default position
 - Load obj from .obj file, process child base on its name, map original image 2D on bottom
 - Load sensor infomation and add into scene
*/
function init() {
    container = document.createElement('div');
    document.body.appendChild(container);

    perspectiveCamera = new THREE.PerspectiveCamera(settings.camera.fov, window.innerWidth / window.innerHeight, .01, 1000);
    var location = {x: -5.671170182101704,
        y: 2.6157304609489826,
        z: -4.073080637934211}
    perspectiveCamera.position.set(location.x, location.y, location.z);

    defaultScene = new THREE.Scene();
    defaultScene.background = new THREE.Color("grey");
    {
        const skyColor = 0xB1E1FF;  // light blue
        const groundColor = 0xB97A20;  // brownish orange
        const intensity = 1;
        const light = new THREE.HemisphereLight(skyColor, groundColor, intensity);
        defaultScene.add(light);
      }
    
      {
        const color = 0xFFFFFF;
        const intensity = 1;
        const light = new THREE.DirectionalLight(color, intensity);
        light.position.set(5, 10, 2);
        defaultScene.add(light);
        defaultScene.add(light.target);
      }

    

    const gltfLoader = new GLTFLoader();
    gltfLoader.load('three-dimension-object/gltf/House_demo/House_demo_separate.gltf', (gltf) => {
      const root = gltf.scene;
      defaultScene.add(root);
      
    setTimeout(function () {
        loadSensor();
    }, 100)
      // compute the box that contains all the stuff
      // from root and below
      const box = new THREE.Box3().setFromObject(root);

      const boxSize = box.getSize(new THREE.Vector3()).length();
      const boxCenter = box.getCenter(new THREE.Vector3());

      // set the camera to frame the box
      //frameArea(boxSize * 0.5, boxSize, boxCenter, camera);

      // update the Trackball controls to handle the new size
      orbitControl.maxDistance = boxSize * 10;
      orbitControl.target.copy(boxCenter);
      orbitControl.update();
    });

    redSphere = new THREE.Mesh(
        new THREE.SphereGeometry(.2, 32, 32), 
        new THREE.MeshBasicMaterial({
            color: 65535
        })
    );
    redSphere.castShadow = true;
    redSphere.receiveShadow = true;        

    webGLRender = new THREE.WebGLRenderer({
        antialias: !0,
        preserveDrawingBuffer: !0,
        alpha: !1
    });
                    
                
    if (settings.ambient) {
        var n = new THREE.AmbientLight(16777215, .9);
        defaultScene.add(n)
    }
    projector = new Projector;
    webGLRender.setPixelRatio(window.devicePixelRatio);
    webGLRender.setSize(window.innerWidth, window.innerHeight);
    webGLRender.shadowMap.enabled = !0;
    webGLRender.gammaOutput = settings.gammaOutput;
    webGLRender.toneMappingExposure = settings.exposure;
    webGLRender.renderReverseSided = !0;
    webGLRender.localClippingEnabled = !0;

    container.appendChild(webGLRender.domElement);

    orbitControl = new OrbitControls(perspectiveCamera, webGLRender.domElement);
    orbitControl.enableDamping = true;
    orbitControl.dampingFactor = 0.55;
    orbitControl.enableZoom = true;
    orbitControl.update();

    window.addEventListener("resize", resize, !1);
    window.addEventListener("click", windowClick, !1);

};

function render() {

    webGLRender.render( defaultScene, perspectiveCamera );

}

function animate(e) {
    requestAnimationFrame(animate);
    webGLRender.render(defaultScene, perspectiveCamera);
    TWEEN.update(e);
    orbitControl.update()
}

/* Get sensor correspond with sensor in api data.
 If it existed: 
 + Create a red sphere by using mesh and push into listSensor
 + Generate list sensor and append to sensorsElement
*/
function loadSensor() {
    var r = 0, sensorsElement = $("#sensors");
    setTimeout(function () {
        positions.forEach(function (pos) {
            var sensor = apidata.find(function (s) {
                return s.sensor_id == pos.sensor_id
            });
            if (sensor) {
                var mesh = redSphere.clone();
                mesh.position.set(pos.position.x, pos.position.y, pos.position.z);
                mesh.userData = sensor;
                mesh.userData.poistionInfo = pos;
                mesh.userData.room = pos.room;
                mesh.userData.index = r;
                var a = ""
                    , color = red
                    , o = "";
                
                sensor.last_measurement.co2 && 700 < sensor.last_measurement.co2 
                && (color = green,
                    o = "warn",
                    a = ' <i class="fa fa-exclamation-triangle"></i>');

                mesh.userData.originalColor = red;
                mesh.material = new THREE.MeshLambertMaterial({
                    color: color
                });
                defaultScene.add(mesh);                
                listSensor.push(mesh);
                sensorsElement.append("<li data-index='" + r + "' class=" + o + '><i class="fa fa-circle ok"></i> ' + sensor.name + a + "</li>");
                r++
            }
        });
        $("#warningList").slideDown(900)
    }, 100)
};

/*
    - Translating from screen coordinates to cartesian coordinate in 3D world
    - Put this poistion below camera viewpoint
    - Create a dicrection from camera position and mouse position. If the direction intersect any sensor => active it.
*/
function windowClick(e) {
    var currentPosition = new THREE.Vector3(e.clientX / window.innerWidth * 2 - 1, 2 * -(e.clientY / window.innerHeight) + 1, .5);
    projector.unprojectVector(currentPosition, perspectiveCamera); //vector.unproject(camera)
    currentPosition.sub(perspectiveCamera.position);
    currentPosition.normalize();
    var matchBubs = new THREE.Raycaster(perspectiveCamera.position, currentPosition).intersectObjects(listSensor, !0);
    var bub;
    0 < matchBubs.length && settings.logIntersects && (bub = matchBubs[0].object,
        setActiveBubMaterialColor(bub),
        activeSensor(bub.userData))
}


function setActiveBubMaterialColor(sensor) {
    listSensor.forEach(function (s) {
        s.material.color = s.userData.originalColor
    });
    sensor.material.color = yellow;
}

/*
- Active sensor in list
- Change (tween) position of sensor and camera
 */
function activeSensor(sensor) {
    bubIndex = sensor.index;
    $("#sensors li").removeClass("active");
    $("#sensors li").eq(bubIndex).addClass("active");    
    sensor.attributes || (sensor = getSensorById(sensor.sensor_id));
    //displaySensorInfo(sensor);
    
    var sensorPosition = sensor.poistionInfo;
    var tweenSensor = new TWEEN.Tween(orbitControl.target)
    .to(sensorPosition.position, 700)
    .onComplete(function () { })
    .easing(TWEEN.Easing.Cubic.Out);
    
    var tweenCamera = new TWEEN.Tween(perspectiveCamera.position)
    .to(sensorPosition.cameraLocation, 700)
    .onStart(function () {
        orbitControl.enabled = !1
    })
    .onComplete(function () {
        orbitControl.enabled = !0
    })
    .easing(TWEEN.Easing.Cubic.Out);

    tweenSensor.start();
    tweenCamera.start()
}

function displaySensorInfo(sensor) {
    var n = $("#datadisplay");
    n.is(":hidden") && n.fadeIn();
    n.find(".device-info").html(sensor.name);
    n.find(".room-info").html(sensor.room);
    var t = "<table>";
    t += "<tr>";
    var a = Object.keys(sensor.last_measurement);
    for (var propIndex in a)
        t += "<td>" + a[propIndex] + "</td>";
    t += "</tr>";
    t += "<tr>";
    Object.values(sensor.last_measurement).forEach(function (e) {
        t = "object" == typeof e ? t + ("<td>" + typeof e + "</td>") : 700 < e ? t + ('<td class="data-warning">' + e + "</td>") : t + ("<td>" + e + "</td>")
    });
    t += "</tr>";
    t += "</table>";
    n.find(".data-container").html(t)
}
function getSensorById(id) {
    var sensor = apidata.find(function (e) {
        return e.sensor_id == id
    });
    if (sensor)
        return sensor
}

$("#closedisplay").click(function () {
    $("#datadisplay").fadeOut()
});

function resize() {
    perspectiveCamera.aspect = window.innerWidth / window.innerHeight;
    perspectiveCamera.updateProjectionMatrix();
    webGLRender.setSize(window.innerWidth, window.innerHeight)
}


$("#sensors").on("click", "li", function () {
    var index = $(this).attr("data-index")
        , position = positions[index];
    bubIndex = index;
    position.index = bubIndex;
    setActiveBubMaterialColor(listSensor[bubIndex]);
    activeSensor(position)
});